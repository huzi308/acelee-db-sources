package com.aceleeyy.action;

import org.springframework.stereotype.Component;

/**
 * Oracle
 *
 * @author Ace Lee
 * @date 2019-08-29 9:35
 */
@Component("ORACLE")
public class OracleQueryStrategy extends AbstractSqlQueryStrategy {

	/**
	 * ORACLE 12c
	 */
	private static String DRIVER = "oracle.jdbc.driver.OracleDriver";

	@Override
	protected String getDriverName() {
		return DRIVER;
	}
}
