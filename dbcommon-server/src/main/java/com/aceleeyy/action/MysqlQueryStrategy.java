package com.aceleeyy.action;

import org.springframework.stereotype.Component;

/**
 * MySQL旧版本
 *
 * @author Ace Lee
 * @date 2019-08-29 9:35
 */
@Component("MYSQL_OLD")
public class MysqlQueryStrategy extends AbstractSqlQueryStrategy {

	/**
	 * MYSQL 5
	 */
	private static final String DRIVER = "com.mysql.jdbc.Driver";

	@Override
	protected String getDriverName() {
		return DRIVER;
	}
}
