package com.aceleeyy.action;

import org.springframework.stereotype.Component;

/**
 * DB2
 *
 * @author Ace Lee
 * @date 2019-08-29 9:35
 */
@Component("DB_TWO")
public class Db2QueryStrategy extends AbstractSqlQueryStrategy {

	/**
	 * DB2
	 */
	private static String DRIVER = "com.ibm.db2.jcc.DB2Driver";

	@Override
	protected String getDriverName() {
		return DRIVER;
	}
}
