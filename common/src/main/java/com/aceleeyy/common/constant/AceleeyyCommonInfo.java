package com.aceleeyy.common.constant;

/**
 * @author Ace Lee
 * @create 2019-2-28 14:50
 * @desc 平台字典列表
 *
 * ----注意常量值大写
 * ----每个字典必须注释清楚含义
 **/
public class AceleeyyCommonInfo {

    /**
     * 是否
     */
    public static final boolean BOOLEAN_YES = true;
    public static final boolean BOOLEAN_NO = false;

    /**
     * 数据库类型
     */
    public static final String DB_TYPE_DM = "DM";
    public static final String DB_TYPE_MYSQL5 = "MYSQL_OLD";
    public static final String DB_TYPE_MYSQL8 = "MYSQL_NEW";
    public static final String DB_TYPE_ORACLE = "ORACLE";
    public static final String DB_TYPE_SQLITE = "SQLITE";
    public static final String DB_TYPE_SQLSERVER = "SQLSERVER";
    public static final String DB_TYPE_DB2 = "DB_TWO";

    /**
     * 用户连接信息缓存key
     */
    public static final String REDIS_KEY_USER_DB = "RK_USER_DB";

    /**
     * 用户连接信息缓存时长
     */
    public static final int REDIS_KEY_USER_DB_TIME = 10*60;//10分钟，单位（秒）

    /**
     * SQL类型
     */
    public static final String SQL_TYPE_INSERT = "insert";//增
    public static final String SQL_TYPE_DELETE = "delete";//删
    public static final String SQL_TYPE_UPDATE = "update";//改
    public static final String SQL_TYPE_SELCTE = "select";//查

}

