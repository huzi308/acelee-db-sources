package com.aceleeyy.common.web.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.CrossOrigin;

/**
 * 父类接口
 *
 * @Author Ace Lee
 * @Date 2019/9/2 9:20
 * @Version 1.0
 **/
@Slf4j
@CrossOrigin(origins = "*", maxAge = 3600)
public abstract class BaseController {

    /**
     * 数据库包含：
     *      数据库连接信息
     *      数据库元数据信息
     *      数据库操作
     *
     * 接口：
     *      1.用户（目前不含有用户）连接数据库
     *              请求：数据库连接信息
     *              响应：数据库元数据信息
     *
     *      2.执行SQL
     *              请求：SQL语句
     *              响应：SQL结果
     *
     *      3.退出/关闭
     *              请求：
     *              响应：关闭成功
     *
     *      4.特殊SQL治理接口
     *              请求：
     *              响应：治理成功
     *
     *      5.数据库连接日志
     *              请求：
     *              响应：连接时间，执行操作
     *
     */













}
