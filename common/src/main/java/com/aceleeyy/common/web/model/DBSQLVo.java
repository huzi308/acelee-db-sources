package com.aceleeyy.common.web.model;

import lombok.Data;

import java.io.Serializable;

@Data
public class DBSQLVo implements Serializable {

    /**
     * SQL
     */
    private String sql;

    /**
     * 类型
     */
    private String type;

    /**
     * 表名
     */
    private String tablename;
}
