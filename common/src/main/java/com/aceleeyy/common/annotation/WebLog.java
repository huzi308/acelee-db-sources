package com.aceleeyy.common.annotation;

import java.lang.annotation.*;

/**
 * 系统日志记录
 *
 * @author Ace Lee
 * @create: 2019-04-11 14:59
 */
@Target({ ElementType.METHOD })
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface WebLog {

}